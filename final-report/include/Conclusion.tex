% CREATED BY DAVID FRISK, 2016
\chapter{Conclusion}

After our tests, we draw our conclusions and evaluate the project in this chapter. In section \ref{sec:concludingRemarks} we discuss what we gained from this project as well as its impact on the state of the art and, lastly, in section \ref{sec:perspectives} we address the limitations of the project and what could be done in the future to correct them.

%=================================================================
\section{Considerations}%=========================================
\label{sec:concludingRemarks}%====================================
%=================================================================

Our overall judgement of the API is that it is an interesting starting point for the introduction of RV on an Android development platform but, at the same time, it requires a way for the user to define any number of properties and monitor them without translating them manually to Kotlin code.

Our approach in making a syntactically similar framework to AspectJ made it easier for us to implement monitors and have them observe specific events within the execution of the app.

We can use our experience with this project to provide answers to our research questions from section \ref{sec:researchQuestions}.

\vspace{5mm}
\textbf{What are the meaningful properties of Kotlin coroutines that we can verify by employing RV?}

\vspace{5mm}
We identified a handful of properties that have to do with creation and termination of coroutines at runtime. This number will likely rise as the currently experimental coroutine functionalities (e.g. the builder methods \texttt{actor} and \texttt{produce} mentioned in section \ref{sec:experimentalCoroutineTypes}) are given a stable implementation. The properties related to what dispatcher is associated with a certain task are also certainly interesting.

\vspace{5mm}
\textbf{Can we use the Kotlin language to monitor properties of the more specific Kotlin features?}

\vspace{5mm}
We could define an API close to AspectJ in its purpose but we couldn't achieve a coverage as wide as we would have wanted. We will elaborate more on this in the section below.

\vspace{5mm}
\textbf{How can we push further the current situation for RV of Android applications by using Kotlin?}

\vspace{5mm}
We feel that there is definitely something to be gained from applying Runtime Verification to an app still in development: this can be used to detect unexpected behaviour, especially when originated from language-specific features with which the developer may not be familiar.

Kotlin has recently been adopted as a main language for Android development, which means its user base is likely going to see a marked increment in the coming years: the monitoring of its unique features, while not necessarily relevant for released apps (as explained at the start of Section \ref{sec:projectSplit}), can be of assistance when migrating old Java code or refactoring classes using an outdated approach.

%=================================================================
\section{Perspectives and Future Work}%===========================
\label{sec:perspectives}%=========================================
%=================================================================

% Unfortunately our API lacks the same coverage of the ``afterReturning/Throwing'' and ``around'' pointcuts featured in AspectJ: monitors that detect whenever a task fails right after its failure would prove quite useful in observing exceptional behaviour despite the different approaches to exception handling defined in the methods \texttt{launch} and \texttt{async}. Likewise, a pointcut designed to allow an operation only under certain conditions would prove extremely powerful when running Runtime Enforcement.

There are several changes that could be made to enhance this final API even further but that we opted not to do as we did not feel the need to carry them out at this time.

Like we mentioned earlier, during our final redesign we tried to make our API more similar (at least in name) to AspectJ: we now have several ``before'' and ``after'' methods and, to provide a more complete range of methods, we would need:
\begin{itemize}
    \item some sort of ``around'' method that decides whether a given task should be started or not and, in the latter case, what should be returned instead;
    \item an ``afterReturning'' method for the \texttt{async} tasks: this should be executed after the given block of code provided that no exception was thrown.
\end{itemize}

These methods could prove an asset to a future version of the monitoring API.

Another enhancement would be a rework of the \texttt{MonitoredApplication} class: as of now it only keeps track of the \texttt{recommendedDispatchers} maps defined for each instance of \texttt{MonitoredComponent} and its methods are very transparent about it. We feel that the class should be given a treatment similar to the \texttt{MonitoredComponent} interface with a rehaul of its exposed methods and the opportunity to save a more varied range of information. Of course, such a modification would need to be tested for its impact on performance and memory consumption.

\vspace{5mm}
As emerged from our test of the memory footprint (see section \ref{sec:memory}), the current way of saving records of dispatcher data employs the class name of each \texttt{MonitoredComponent} instance. This makes the combined length of class and package name influence the amount of memory occupied by our application and its data structures. The class name is, however, currently used by the \texttt{TerminationService} class to print a human-readable list of all recommended coroutine dispatchers grouped by their lifecycle components; we need therefore to find an alternate way to store records of each component without using their class name but at the same time keeping them recognisable by a human programmer.

\vspace{5mm}
Despite all this, the main drawback in our implementation was the aforementioned need to define properties as blocks of Kotlin code. \textbf{Our API needs an interpreter to translate properties to Kotlin code} instead of forcing this task onto the developer: a solution would be the employment of an already existing specification language like DATEs \cite{LARVAtoolpaper} with the creation of an intermediate layer carrying out the parsing and interpretation.

Another important limitation of our tool is its coverage of the sole source code, since its instrumented versions of \texttt{launch} and \texttt{async} do not fully affect the code used in libraries.