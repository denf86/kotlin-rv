% CREATED BY DAVID FRISK, 2016

%#################################################################
\chapter{Introduction}%###########################################
%#################################################################

%This chapter presents the section levels that can be used in the template. 

Throughout the last decade smartphones have become a huge part of everyday life, growing from portable telephones to complex computers with which users can not only communicate but also access their bank account, monitor their health, play video games, find their car in a parking area and more. 

All sorts of applications can be found in virtual stores for Android and iOS and, in turn, there is a growing need to guarantee that they pose no threat to the user's smartphone by means of a faulty implementation (e.g. an app consuming more battery than needed) or malicious operations (e.g. an app leaking personal data).

\textbf{Runtime Verification} \cite{havelund05,leucker09,falcone13,bartocci18,lecturesOnRV2018} is one of the possible methods of monitoring smartphone applications and reporting unwanted behaviour: it consists of a series of checks that verify at runtime whether the target program complies with a set of user-defined properties. In the case of devices using Google's Android operating system, there have been several attempts to employ RV (e.g. \cite{LARVAtoolpaper,falcone12,sun17}). The Android operating system is however constantly changing, with Android 10 \cite{android10} being the newest version at the time of writing, and monitoring tools have to stay up to date with the more recent releases.

With this project we want to observe the current state of Runtime Verification on Android and determine how it can be pushed further. Aiming to improve the state of the art, we focused our efforts on the newly supported \textbf{Kotlin language} \cite{kotlinFAQ}: more precisely, we wished to investigate how its unique features over Java could be monitored.

The project was developed in cooperation with \textbf{Opera Software AB} \cite{opera}, a Norwegian software company primarily known for its desktop web browser Opera, its mobile web browser Opera Mini and its recent shift to mobile development with fintech applications for Android (e.g. \cite{otouch, onews, opay}). Opera Software, or just “Opera”, was founded in 1995 and to this day has headquarters in Oslo, Norway, and offices in Poland (Wrocław), Sweden (Linköping, Göteborg, Stockholm) and China (Beijing), focusing on browsers for most of its existence and promoting Web standards through participation in the W3C.

%=================================================================
\section{Problem Statement}%======================================
\label{sec:definitionRvAndInstrumentation}%=======================
%=================================================================

\textbf{Runtime Verification} is one of several methods used for ensuring the correctness of programs. It involves the monitoring of a running program or system while observing that it complies with one or more given properties during its execution; a report is then generated detailing whether any property has been violated.

Runtime Verification, or ``RV'', can be split in two major tasks:
\begin{enumerate}
    \item \textbf{defining the properties} that the system must comply with: they will be expressed with a statement that will be more or less complex depending on their nature;
    \item \textbf{generating the monitors} that will verify these properties: this is often platform-dependent as it involves modifying a program or wrapping it in an observable layer.
\end{enumerate}



% %-----------------------------------------------------------------
% \subsection{Definition of Properties}%----------------------------
% %-----------------------------------------------------------------

% The nature and the goal of a program can determine what properties should be considered for monitoring. Some programs handle personal data and it is therefore critical to ensure this data is not leaked; some other programs carry out money transactions and they need to be robust enough to handle failures with minimal impact.

% Properties can also involve the platform on which a program is run and the impact it has on the platform's resources. Properties can be expressed at different levels of abstraction as well as combine platform-specific issues with general concerns like security: for example, limiting data access of a company app running on an BYOD smartphone of an employee \cite{buttigieg15}.

% A more complex kind of properties is the one involving conditions: a given property should be verified only in a given context which can be of mixed nature:
% \begin{itemize}
%     \item an operation that can only be performed a given number of times, e.g. send up to five notifications a day to avoid spam;
%     \item an operation that invalidates another, e.g. after asking geo-location data a smartphone app can no longer send server requests to avoid exfiltration;
%     \item an operation that can be allowed or forbidden depending on the context in which it is executed, e.g. carry out a large download only through a WiFi connection.
% \end{itemize}

% The more complex properties are, the more they require an expressive language to represent them. While some simple ``if \textit{A}, then \textit{B}'' properties can most likely be written in a straightforward way, others like the examples given in the previous paragraph require a more articulate formula. Previous works on this topic have used both \textbf{logic-based approaches} (mostly based on variations of linear temporal logic \cite{bauer12}) and \textbf{finite-state automata} \cite{colombo09} to represent stateful properties and traces:
% \begin{itemize}
%     \item each relevant operation performed by the program is associated with an \textbf{event}, e.g. ``send a notification'';
%     \item each observable state of the execution is associated with a \textbf{state} of the automaton, e.g. ``notifications allowed'' or ``notifications restricted'';
%     \item the future state of the automaton can change depending on a combination of its current state and the next event to occur.
% \end{itemize}

% The final bullet point can be needlessly complex in some cases, like the ``no more than five notifications a day'' property mentioned above: normally this would require a different state for any given number $n$, $0 \leq n \leq 5$ of notifications sent within a day. If the limit was relaxed to 10 or even 100 notifications the number of states would increase accordingly, which is neither desirable or correct as it increases the complexity of the automaton according to an arbitrary threshold rather than the monitored property.

% By storing parts of the state in dedicated \textbf{variables} it is possible to cut an the amount of states of an automaton to a more manageable number: using the same example we would now have two states (an ``OK'' state and a ``bad'' state where notifications are forbidden) with the transition from \textit{OK} to \textit{bad} being triggered by the combination of the ``send a notification'' event and a ``number of notifications sent today'' variable having value equal to 5.

% %-----------------------------------------------------------------
% \subsection{Integration of Monitors}%-----------------------------
% %-----------------------------------------------------------------

% Monitors are, by nature, wrappers that add a layer of observation to the target program; this means that they are programs themselves, written using either the same technology as the target or a different one. They are also not necessarily written together with the target program, since RV ideally sees any system as a ``black box''. This makes monitors a possibly entirely separate entity from the target program.

When monitors are not part of the target system but, rather, a second program, a need arises to connect the monitors to the system in such a way they can effectively observe the execution. The operation of detecting traces of events from the target program and relaying them to a monitoring system is known as \textbf{instrumentation} \cite{bartocci18} and can be carried out on the target program's binary code or directly on its source code.

% \subsubsection{Instrumentation}%----------------------------------
% %-----------------------------------------------------------------

% The paradigm of \textbf{Aspect-Oriented Programming} is focused on separating ``cross-cutting concerns'', which are anything that influences multiple functionalities in a program like error reporting or security. Monitors fit quite well in this category, so AOP is naturally seen as an ideal way of implementing them.

% While several languages can implement AOP, we want to focus on Java to give a more concrete example. The Java implementation of this paradigm is called \textbf{AspectJ} \cite{aspectj} and employs a dedicated compiler to ``read'' directives and ``inject'', or ``weave'', them into the target program.

% AspectJ directives are called \textbf{aspects} and are composed of several parts:
% \begin{itemize}
%     \item a \textbf{join point} is any point during execution that can be clearly identified, like a method call, an instantiation or a value being returned;
%     \item a \textbf{pointcut} is a filter or a set of filters that identify specific join points;
%     \item an \textbf{advice} is a method that is executed \textit{before}, \textit{after} or \textit{around} a given pointcut.
% \end{itemize}

% An aspect can be written either as a Java class by using annotations like \texttt{@Aspect}, or as an ``aspect file'' using the unique syntax defined for AspectJ. Aspects can also contain fields to be used as state variables, or define additional fields and methods for a given class. These directives are parsed by the AspectJ compiler and applied to the target Java application.

% The output of the AspectJ compilation is a new version of the original application, with the monitoring code ``woven'' into it. This means that the new program will actively notify the monitors about any relevant events taking place, making the original source code and the monitoring code one and the same in the eyes of the JVM.

%-----------------------------------------------------------------
\subsection{RV of Android}%---------------------------------------
%-----------------------------------------------------------------

Since the introduction of the \textbf{Android operating system}, there has been a growing interest in applying RV to it. This has spawned several research efforts throughout the past years (e.g. \cite{daian15, LARVAtoolpaper, sun17}). For most of Android's lifetime, its main language for development has been Java; naturally, most attempts to introduce RV to Android have seen the employment of Java-specific tools (e.g. \cite{aspectj}) to instrument applications.

% Since the introduction of the Android operating system, Java has been its preferred language for development, with the consequence that most attempts to introduce RV to Android have seen the employment of AspectJ. There are several tools as of today that can convert a set of properties into AspectJ code to be woven into an Android application. Some examples are:
% \begin{itemize}
%     \item \textbf{RV-Android} \cite{daian15} allows users to choose previously-defined properties from a data base;\\
%     \item \textbf{LARVA} \cite{colombo09} employs DATEs (Dynamic Automata with Events and Timers) to express more complex properties;
%     \item \textbf{ADRENALIN-RV} \cite{sun17} defines properties in a domain-specific language called DiSL.
% \end{itemize}

% Some monitoring tools are Android applications themselves that, given a target app and a set of properties, instrument the target by compiling it with the AspectJ compiler and install the instrumented version over the old one. Other tools are much more invasive and require modifications to the Android runtime installed into the device.

% Any application running on Android is initially written in Java or Kotlin, then compiled and converted into \textit{Dalvik bytecode} so it can run in the Android operating system. It is possible to convert Dalvik into Java bytecode through an external tool, for example \textit{dex2jar} \cite{dex2jar}; this Java bytecode can subsequently be recompiled by the AspectJ compiler to inject monitoring code. This allows any application for Android devices to be instrumented.

% Given the possibility of instrumenting potentially any application, the focus is then on the properties to be monitored. They can depend on several factors:
% \begin{itemize}
%     \item the device itself, e.g. battery consumption or geolocation;
%     \item general features of smartphone apps, e.g. memory consumption;
%     \item specific features of an app or a category of apps, e.g. handling crashes;
%     \item how apps use inter-process communication, e.g. sending and receiving the user's personal data;
%     \item the language that the app is written in, specifically with regard to general guidelines and design patterns.
% \end{itemize}

% The last two bullet points have proven to be interesting fields to look into for our thesis project. We decided to further our research into the latter, taking advantage of the Kotlin language which has been recently chosen as the ``preferred'' development language for Android applications \cite{kotlinFAQ}.

% %-----------------------------------------------------------------
% \subsection{Kotlin on Android}%-----------------------------------
% %-----------------------------------------------------------------

As of 2017, the status of preferred development language for Android has shifted to \textbf{Kotlin}, a more recent programming language that can interoperate with Java while also introducing a set of unique features.

% Kotlin is a rather new programming language, developed in 2011 but only supported for Android development since 2017. The language is built with a syntax similar to that of Java and the ability to interoperate with it: this allows any Kotlin program to execute Java code and employ Java libraries. 

% Being a cross-platform language, Kotlin can compile to JVM bytecode, JavaScript or, through LLVM compilation, native binaries for C, Swift and more. It is therefore possible to write both iOS and Android applications by employing, respectively, LLVM and JVM compilation: this will of course be our main area of interest.

There are a number of differences between Java and Kotlin; the most relevant for our research are:
\begin{itemize}
    \item \textbf{checked exceptions}: compared to Java, Kotlin does not force the programmer to handle expected exceptional behaviour; when an API can be expected to throw an exception, the Kotlin compiler will raise no error or warning. Behind this choice is a push to favour special return types instead of exceptions \cite{javaExceptionsBad}, when possible;
    \item \textbf{coroutines}: introduced as a concept in the 1960s \cite{firstCoroutine}, they are lightweight tasks that can run concurrently inside threads; they do not exist in Java and Kotlin introduced them as an alternative to callbacks \cite{callback} in Android development, especially for dealing with lifecycle-aware structures.
\end{itemize}

We found that most of the recommended usage practices of Kotlin can be monitored by means of static analysis.

On the other hand, coroutines are quite different. In Kotlin they are intended for use with structured concurrency \cite{structuredConcurrency}, which means entry and exit points must be made clear and all tasks are either completed or cancelled before the end of the execution. This approach, coupled with Kotlin's choice to not enforce exception handling, means that a whole coroutine scope will be terminated should any task crash \cite{coroutineExceptions}.

Coroutines are designed with the goal of running either attached to a single thread or moving from one thread to another. They are implemented as stackless, which reduces the overhead caused by saving the current state between each thread jump, and can move between main and secondary threads depending on the task they need to carry out.

We deemed these aspects of coroutines to be interesting to investigate in our project to further RV on Android.

%=================================================================
\section{Goals and Challenges}%===================================
\label{sec:researchQuestions}%====================================
%=================================================================

In this thesis project we aimed to define a RV framework for Android applications with the three main tasks of:
\begin{enumerate}
    \item expressing a range of system properties as wide as possible;
    \item making the monitoring system translate said properties into meaningful Android code without loss of information;
    \item defining a feedback to be returned by the monitoring system in the form of either a corrective action or a report produced post-execution.
\end{enumerate}

With this framework we aimed to address the research questions expressed below.

\vspace{5mm}
\textbf{What are the meaningful properties of Kotlin coroutines that we can verify by employing RV?}

\vspace{5mm}
We decided to focus our efforts in coroutines, which are well-suited for Runtime Verification and Enforcement. We could inspect official guidelines, design patterns and recommended practices to identify examples of good app behaviour and transform these examples into properties to observe or enforce.

\vspace{5mm}
\textbf{Can we use the Kotlin language to monitor properties of the more specific Kotlin features?}

\vspace{5mm}
We set out to use the Kotlin language to either adapt an existing tool for RV of Android or create our own. This process would ideally allow us to maximise our ability to monitor Kotlin properties.

\vspace{5mm}
\textbf{How can we push further the current situation for RV of Android applications by using Kotlin?}

\vspace{5mm}
We would evaluate our framework to determine what possible use cases it could suit best and how it could advance the state of the art for RV of Android. Our evaluation would include testing the way that properties are observed as well as measuring additional impacts of our solution to the target app's execution; for example, performance overheads caused by the monitoring effort.

\vspace{5mm}
Our research question would be addressed by using the \textbf{Design Research} methodology \cite{hevner04}: we would formulate hypotheses on the problem at hand and, subsequently, design a solution for each hypothesis and implement it into an artefact. This, once tested, would turn out to either validate or invalidate the starting hypothesis.

We would follow this procedure iteratively for each of our research questions.

% In order to carry out these tasks we needed to identify which tools, among the ones currently existing, would be most suited to carry out points 1 and 2. We set therefore a sub-goal of \textbf{evaluating the currently available RV tools for Android}: each would be evaluated on the expressiveness of its specification language for properties (point 1) and the correctness it could achieve in the monitoring phase (point 2), i.e. how well it would translate properties to code and how much of the execution it could cover.

% The Kotlin language is still new to the Android development scene so we identified the possibility that none of the existing RV tools would prove suitable. We decided that, in that case, we would set the \textbf{additional goal} of defining our own, either by adapting another tool or by creating a new one.

% Keeping the OPay app as a possible target for RV we identified some security properties typical of transaction-oriented systems: the user’s access PIN code never being saved as persistent data on the device or transactions being forbidden unless the user is correctly authenticated, among others.

% Like several apps on the financial spectrum OPay uses a ``Know Your Customer'' (KYC) approach \cite{kyc} with its user base where new users are restricted in what they can do and their range of possible operations increases when they meet certain requirements such as e.g. email verification. It was reasonable for us to identify another monitorable property as: a new, not-yet-verified user should not be able to carry out actions above their current KYC level, e.g. sending a high amount of money requests to other users.

% Other properties to monitor were given by the Android system independently on which application we would decide to target.

% The nature of Android is that of a permission-based framework: applications can’t carry out certain ``risky'' actions without expressively requesting permissions from the user \cite{permissions}. Some of these permissions include reading the device’s unique IMEI (or ``International Mobile Equipment Identity'') code, tracking the current geographical location and accessing the device storage. We reasoned that it would be beneficial to know that these permissions were used to execute specific actions inside the app and were not limited to, for example, libraries from third parties; it would likewise be a good idea to give limited access to persistent storage on the user’s device.

% Some examples, still assuming that a financial app be chosen as target application, were considered as prototypes before we began our work; they are listed below, formatted as expression of the type\\ 
% \indentcm $e: c \rightarrow a$
% \\where $e$ is an event that, when detected by a monitor, triggers the evaluation of the condition $c$: when this condition is met, the monitors carries out the action $a$.
% \begin{itemize}
%     \item authentication token is lost mid-session: user is attempting a transaction $\rightarrow$ the transaction is aborted;
%     \item user logs out of OPay: user had logged into the app via PIN code $\rightarrow$ PIN code is removed from the application's memory;
%     \item user attempts to request money from another user: (user KYC level is below a set threshold) AND (user has requested money a set number of times) $\rightarrow$ money request is denied and user is asked to upgrade the KYC level.
% \end{itemize}
% As stated earlier, we would also be looking for properties more tied to Kotlin and, in particular, its coroutines. They are supposed to be used for concurrent tasks and for replacing callbacks so they need to be robust and predictable, which means they need to handle failures without causing damage to the user and their behaviour needs to be as straightforward as possible. With these ideas in mind we identified some language-specific properties as examples. They are listed below, with the same notation as the previous list.
% \begin{itemize}
%     \item a lifecycle component is destroyed: one or more coroutines in its scope are running $\rightarrow$ all tasks are cancelled;
%     \item an asynchronous computation is carried out: no exceptions were thrown during its execution $\rightarrow$ the computation yields either a value or a notice of cancellation;
%     \item an asynchronous computation is terminated: some exceptions were thrown during its execution $\rightarrow$ the computation throws either the exception causing the failure or some other related error;
%     \item a slow task (either an I/O operation or a long computation) is launched: the enclosing scope is that of the UI thread $\rightarrow$ the task is not launched on the UI thread but on the ``correct'' one.
% \end{itemize}

% We would need these properties to execute the third and final task, which was the definition of a feedback of sorts to deliver to the target application. It would be carried out by evaluating the properties one by one. Each property would be made into a case of study and, where possible, given one or more ``corrective'' actions that the monitoring system needed to carry out to detect and report a violation or even to enforce that the property be followed.

% %=================================================================
% \section{Approach Overview}%======================================
% %=================================================================

% We needed to define our platform of choice for testing our eventual implementation: one option was using a controlled environment, such as an Android emulator, while another was employing one or more physical devices running the Android OS. We decided that we should evaluate both methods for their pros and cons, and eventually select one of them to be used consistently throughout the whole process.

% Our approach followed the \textbf{Design Research} methodology \cite{hevner04}. We aimed to formulate hypotheses on the problems at hand, i.e. the aforementioned system properties or other properties pertinent to the OPay application; for each hypothesis a solution would be formulated and later implemented into a resulting artefact. This, once tested, would turn out to either validate or invalidate the starting hypothesis.

% We executed this procedure iteratively for each property that we set out to monitor.

% %=================================================================
% \section{Time Schedule}%==========================================
% %=================================================================

% %The time plan should give an approximate date when the work is to be finished. It should also list mandatory seminars and milestones for the project with dates for critical steps that are needed to finish the work (intermediate and final report, presentation, opposition etc).
% %The time plan can be updated if and when needed, always in collaboration with your supervisor and upon approval of the examiner.

% The project was started on September 2019 and would last for approximately 20 weeks, thus ending in January 2020.

% During September and the first half of October we observed the state of the art for Android RV. For this purpose we had the help of Dr. Christian Colombo from the University of Malta who had previous experience on both RV with Java and, more specifically, RV on Android.

% After assessing the situation we evolved our initial goal: from focusing on the sole Kotlin language we set out to to either verify Kotlin implementations or shift to the verification of multi-process apps. Our next step was choosing which of the two topics we should investigate further.

% With this goal we started a joint cooperation with Dr. Yliès Falcone, from University Grenoble Alps, in which we examined the functionalities of Kotlin as a language and how it is different from Java.

% We found several potential topics to explore, among which we identified coroutines as an interesting topic for runtime verification, both in general and on Android in particular. Therefore we chose to expand the first topic, or ``improving the current state of the art for Kotlin implementations on Android''.

% In the second half of November, as the project reached its halfway point, we began to define the properties that we wanted to monitor; we also needed to find a suitable app to carry out our tests on.

% A time plan was produced where the remainder of November would be spent defining all properties and finding an app viable for our tests or, in the worst case, creating one ourselves. The whole of December would be dedicated to searching for a tool we could use for the purpose of performing RV on the target app and then January 2020 would be split between running our tests and writing the present report.

% We could not manage find an app that would meet our requirements and were therefore forced to make one ourselves: this gave us more freedom in creating test cases but at the same time slowed down our progress: despite the initial estimate of finishing the project in January 2020, we found ourselves in need of some more time to both produce a meaningful monitoring system and run all of our intended tests. 

% The project ended up being slightly delayed, with a new end date set for February 2020.

This project can be split in two major phases: a theoretical one for observing the state of RV on the Android platform and a more practical one for contributing to the current situation.

The \textbf{first phase}, which covered the first half of the project, consisted in us assessing the situation on multiple fronts:
\begin{itemize}
    \item investigating the state of Android RV gave us an overview of what had already been tried and what had not yet been accomplished;
    \item examining the unique features of the Kotlin language, as well as its similarities and differences with Java, helped us single out what could be interesting for our research.
\end{itemize}

At the conclusion of the first phase we could determine that not only was the situation of application-centric RV quite advanced for Java Android applications but it also was for Kotlin ones. Android applications run on Dalvik bytecode, which is converted from JVM bytecode: since Kotlin on Android compiles to JVM, any monitor that works on a Java application will therefore also work on a Kotlin one.

Our investigation presented us with a choice to focus on either delving deeper into RV with little concern for development language or shifting our main focus to Kotlin. We opted for the latter and chose coroutines as our main interest with the following motivations:
\begin{itemize}
    \item \textbf{multithreading} is an important topic in Android development: multiple apps and services are always running in either the foreground or the background and often multiple threads are used within the same application; several APIs have been developed to improve the control a programmer has over a multithreaded environment and coroutines contribute in their own unique way;
    \item we found several limitations with the current \textbf{debugging tools} for coroutines, especially on Android where the debug agent is not supported due to a lack of libraries;
    \item while it is possible to implement coroutines in Java \cite{javaCoroutines}, they are so far only a \textbf{language feature} in Kotlin and, as such, are expected to be expanded in the future and receive continued support by JetBrains.
\end{itemize}

The goal of our project turned therefore from ``improving the state of the art of Android RV'' to a more specific ``expanding Android RV to coroutines for both monitoring a concurrent environment and strengthening the available debugging tools'', with our attention turning from the final user of an Android application to the developer.

After establishing the final scope of our project, we moved onto the \textbf{second phase}, which consisted of actually implementing our solution.