package com.android.rv

import android.net.Uri
import com.android.rv.data.Image
import com.android.rv.data.SearchMode
import com.android.rv.data.SearchSettings
import org.json.JSONObject

fun createFlickrUri(settings: SearchSettings) = customFlickrUri(
    settings.tags,
    if (settings.mode == SearchMode.ANY_TAG) "any" else "all"
)

fun customFlickrUri(tags: String, tagmode: String) = Uri
    .parse("https://api.flickr.com/services/feeds/photos_public.gne").buildUpon()
    .appendQueryParameter("tags", tags)
    .appendQueryParameter("tagmode", tagmode)
    .appendQueryParameter("format", "json")
    .appendQueryParameter("nojsoncallback", "1")
    .build().toString()

/**
 * Processes the input [String] and, if it's valid JSON, returns a list of [Image] objects.
 * Note: [Result] can't be a return type, so we have to wrap it.
 * For this reason, instead of returning a [Result]<[List]<[Image]>> we will return a
 * [List]<[Result]<[Image]>> where the failure case is a single-element [List] containing a failure.
 * @param json containing a list of image entries
 * @param retrieveAllData if false (default) retrieve title and image URL;
 *  if true retrieve author, tags and direct link as well
 * @return if [json] is valid, a [List] of [Result] entries containing each [Image];
 *  otherwise, a [List] containing a single [Result.failure] entry.
 */
fun parseFlickrImageJson(json: String, retrieveAllData: Boolean = false) = try {
    val items = JSONObject(json).getJSONArray("items")
    val images = arrayListOf<Result<Image>>()
    for (i in 0 until items.length()) (items.getJSONObject(i)).let {
        val title = it.getString("title")
        val image = it.getJSONObject("media")
        val link = image.getString("m").replaceFirst("_m.jpg", "_b.jpg")
        images.add(
            if (retrieveAllData) {
                val author = it.getString("author")
                    .replaceFirst("nobody@flickr.com (\"", "")
                    .replaceFirst("\")", "")
                val tags = it.getString("tags")
                Result.success(Image(title, author, tags, link))
            } else Result.success(Image(title, link))
        )
    }
    images
} catch (e: Exception) {
    listOf(Result.failure<Image>(e))
}