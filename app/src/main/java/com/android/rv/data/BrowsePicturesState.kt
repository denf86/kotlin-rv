package com.android.rv.data

import com.android.rv.UIState

sealed class BrowsePicturesState : UIState {
    object Init : BrowsePicturesState()
    data class Ok(val settings: SearchSettings, var images: List<Image>) : BrowsePicturesState()
    data class Error(val errorMessage: String) : BrowsePicturesState()
}