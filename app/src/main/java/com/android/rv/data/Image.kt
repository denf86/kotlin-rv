package com.android.rv.data

data class Image(
    var title: String,
    var author: String?,
    var tags: String?,
    var link: String
) {
    constructor(title: String, link: String) : this(title, null, null, link)
}