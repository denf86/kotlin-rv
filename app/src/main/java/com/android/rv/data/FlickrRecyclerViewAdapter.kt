package com.android.rv.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.rv.R
import com.android.rv.hide
import com.android.rv.setBrokenLink

class FlickrImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var thumbnail: ImageView = view.findViewById(R.id.thumbnail)
    var title: TextView = view.findViewById(R.id.title)
    var author: TextView = view.findViewById(R.id.author)
    var tags: TextView = view.findViewById(R.id.tags)
    var separator: View = view.findViewById(R.id.separator)
}

abstract class FlickrRecyclerViewAdapter(
    protected var imageList: List<Image>
) : RecyclerView.Adapter<FlickrImageViewHolder>() {
    override fun getItemCount(): Int = if (imageList.isNotEmpty()) imageList.size else 1

    fun loadNewData(newList: List<Image>) {
        imageList = newList
        notifyDataSetChanged()
    }

    fun displayError(errorMessage: String) {
        imageList = listOf(Image(errorMessage, ""))
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FlickrImageViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.browse, parent, false)
    )

    override fun onBindViewHolder(holder: FlickrImageViewHolder, position: Int) =
        if (imageList.isEmpty()) emptyHolder(holder)
        else bindImage(holder, imageList[position])

    private fun emptyHolder(holder: FlickrImageViewHolder) = holder.run {
        thumbnail.setBrokenLink()
        title.setText(R.string.browse_no_results)
        hide(author, tags)
        separator.visibility = View.GONE
    }

    abstract fun bindImage(holder: FlickrImageViewHolder, image: Image)

    override fun getItemViewType(position: Int): Int = position

    override fun getItemId(position: Int): Long = position.toLong()
}