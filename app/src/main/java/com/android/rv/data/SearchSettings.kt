package com.android.rv.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchSettings(
    val tags: String,
    val mode: SearchMode,
    val compact: Boolean
) : Parcelable

enum class SearchMode {
    ALL_TAGS,
    ANY_TAG
}