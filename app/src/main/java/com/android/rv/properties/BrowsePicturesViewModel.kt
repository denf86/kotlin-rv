package com.android.rv.properties

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.rv.StatefulViewModel
import com.android.rv.createFlickrUri
import com.android.rv.data.BrowsePicturesState
import com.android.rv.data.Image
import com.android.rv.data.SearchSettings
import com.android.rv.monitors.MonitoredViewModel
import com.android.rv.monitors.async
import com.android.rv.monitors.launch
import com.android.rv.parseFlickrImageJson
import com.android.rv.randomDispatcher
import com.android.rv.setTo
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class BrowsePicturesViewModel(
    application: Application
) : MonitoredViewModel(application), StatefulViewModel<BrowsePicturesState> {
    private val _state = MutableLiveData<BrowsePicturesState>(BrowsePicturesState.Init)
    override val state: LiveData<BrowsePicturesState> = _state

    override val defaultHandler = CoroutineExceptionHandler { _, throwable ->
        Log.e("BrowsePicturesViewModel", "Entering error state", throwable)
        GlobalScope.launch(Dispatchers.Main) { errorState(throwable.toString()) }
    }

    fun prepareSearch(settings: SearchSettings?) = when (settings) {
        null -> errorState("Invalid search settings")
        else -> doSearch(settings)
    }

    private fun doSearch(settings: SearchSettings) = createFlickrUri(settings).let { uri ->
        val viewModel = this
        CoroutineScope(randomDispatcher()).launch(viewModel) {
            launch(viewModel, randomDispatcher()) {
                launch(viewModel, randomDispatcher()) {
                    getImages(uri, settings)
                }
            }
        }
        Unit
    }

    private suspend fun getImages(uri: String, settings: SearchSettings) = coroutineScope {
        async(this@BrowsePicturesViewModel) {
            URL(uri).readText()
        }.await().let { data: String ->
            val parsedData = async(this@BrowsePicturesViewModel) {
                parseFlickrImageJson(data, !settings.compact)
            }.await()
            // Set new state based on result.
            launch(Dispatchers.Main) {
                if (parsedData.any { it.isFailure }) errorState("Error reading data from $uri")
                else okState(settings, parsedData.map { it.getOrThrow() })
            }
        }
    }

    private fun okState(settings: SearchSettings, images: List<Image>) =
        _state.setTo(BrowsePicturesState.Ok(settings, images))

    private fun errorState(errorMessage: String) =
        _state.setTo(BrowsePicturesState.Error(errorMessage))
}