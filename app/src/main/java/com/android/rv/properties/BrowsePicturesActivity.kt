package com.android.rv.properties

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.rv.R
import com.android.rv.data.BrowsePicturesState
import com.android.rv.data.SearchSettings
import com.android.rv.getMonitoredViewModel
import com.android.rv.hide
import com.android.rv.monitors.MonitoredActivity
import com.android.rv.properties.data.MonitoredFlickrRecyclerViewAdapter
import com.android.rv.show
import kotlinx.android.synthetic.main.activity_browse_pictures.*
import kotlinx.android.synthetic.main.content_main.recycler_view
import kotlinx.coroutines.CoroutineExceptionHandler

/**
 * Adapted from Tim Buchalka's own version on his Udemy Kotlin/Android course.
 */
class BrowsePicturesActivity : MonitoredActivity() {
    override val defaultHandler = CoroutineExceptionHandler { _, t ->
        Log.e("BrowsePicturesActivity", t.toString())
    }

    override fun onCreate(savedInstanceState: Bundle?) = super.onCreate(savedInstanceState).also {
        setContentView(R.layout.activity_browse_pictures)

        val viewModel = getMonitoredViewModel<BrowsePicturesViewModel>()
        val criteria = intent.getParcelableExtra(INTENT_EXTRA_SEARCH_SETTINGS) as SearchSettings?
        val flickrRecyclerViewAdapter = MonitoredFlickrRecyclerViewAdapter(arrayListOf(), viewModel)

        supportActionBar?.run {
            title = getString(R.string.title_activity_browse)
            subtitle = getString(R.string.browse_searching_tags, criteria?.tags)
            setDisplayHomeAsUpEnabled(true)
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@BrowsePicturesActivity)
            adapter = flickrRecyclerViewAdapter
        }

        viewModel.state.observe(this, Observer { newState ->
            when (newState) {
                is BrowsePicturesState.Error -> {
                    supportActionBar?.title = getString(R.string.title_activity_browse)
                    supportActionBar?.subtitle = null
                    flickrRecyclerViewAdapter.displayError(
                        getString(R.string.browse_error_searching, newState.errorMessage)
                    )
                    hide(nowLoading)
                }
                is BrowsePicturesState.Ok -> {
                    supportActionBar?.run {
                        title = getString(
                            R.string.title_activity_browse_results,
                            newState.images.size
                        )
                        subtitle = getString(
                            R.string.browse_images_found,
                            newState.settings.tags
                        )
                    }
                    flickrRecyclerViewAdapter.loadNewData(newState.images)
                    hide(nowLoading)
                }
                else -> show(nowLoading)
            }
        })

        viewModel.prepareSearch(criteria)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> true
    }

    override fun onBackPressed() = finish()
}

const val INTENT_EXTRA_SEARCH_SETTINGS = "SearchSettings"