package com.android.rv.properties.data

import android.view.View
import com.android.rv.R
import com.android.rv.data.FlickrImageViewHolder
import com.android.rv.data.FlickrRecyclerViewAdapter
import com.android.rv.data.Image
import com.android.rv.loadFrom
import com.android.rv.monitors.MonitoredComponent
import com.android.rv.setTextOrHide

class MonitoredFlickrRecyclerViewAdapter(
    imageList: List<Image>,
    private var lifecycleComponent: MonitoredComponent
) : FlickrRecyclerViewAdapter(imageList) {
    override fun bindImage(holder: FlickrImageViewHolder, image: Image) = holder.run {
        thumbnail.loadFrom(image.link, lifecycleComponent)
        title.text = image.title
        author.setTextOrHide(R.string.browse_author_format, image.author)
        tags.setTextOrHide(R.string.browse_tags_format, image.tags)
        separator.visibility = if (imageList.size < 2) View.GONE else View.VISIBLE
    }
}