package com.android.rv.unmonitored

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.rv.StatefulViewModel
import com.android.rv.createFlickrUri
import com.android.rv.data.BrowsePicturesState
import com.android.rv.data.Image
import com.android.rv.data.SearchSettings
import com.android.rv.parseFlickrImageJson
import com.android.rv.randomDispatcher
import com.android.rv.setTo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL

class BrowsePicturesUnmonitoredViewModel : ViewModel(), StatefulViewModel<BrowsePicturesState> {
    private val _state = MutableLiveData<BrowsePicturesState>(BrowsePicturesState.Init)
    override val state: LiveData<BrowsePicturesState> = _state

    fun prepareSearch(settings: SearchSettings?) = when (settings) {
        null -> errorState("Invalid search settings")
        else -> doSearch(settings)
    }

    private fun doSearch(settings: SearchSettings) {
        viewModelScope.launch {
            var completed = false
            try {
                getImages(createFlickrUri(settings), settings)
                completed = true
            } finally {
                Log.d("UnmonitoredViewModel", "terminating com.android.rv.monitors.launch; completed=$completed")
            }
        }
    }

    private suspend fun getImages(uri: String, settings: SearchSettings) = coroutineScope {
        try {
            withContext(randomDispatcher()) {
                URL(uri).readText()
            }.let { data: String ->
                val parsedData = withContext(randomDispatcher()) {
                    parseFlickrImageJson(data, !settings.compact)
                }
                // Set new state based on result.
                withContext(Dispatchers.Main) {
                    if (parsedData.any { it.isFailure }) errorState("Error reading data from $uri")
                    else okState(settings, parsedData.map { it.getOrThrow() })
                }
            }
        } catch (e: Exception) {
            errorState(e.toString())
        }
    }

    private fun okState(settings: SearchSettings, images: List<Image>) =
        _state.setTo(BrowsePicturesState.Ok(settings, images))

    private fun errorState(errorMessage: String) =
        _state.setTo(BrowsePicturesState.Error(errorMessage))
}