package com.android.rv.unmonitored

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.rv.R
import com.android.rv.data.BrowsePicturesState
import com.android.rv.data.SearchSettings
import com.android.rv.getViewModel
import com.android.rv.hide
import com.android.rv.properties.INTENT_EXTRA_SEARCH_SETTINGS
import com.android.rv.show
import com.android.rv.unmonitored.data.UnmonitoredFlickrRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_browse_pictures_unmonitored.*
import kotlinx.android.synthetic.main.content_main.recycler_view

/**
 * Adapted from Tim Buchalka's own version on his Udemy Kotlin/Android course.
 */
class BrowsePicturesUnmonitoredActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) = super.onCreate(savedInstanceState).also {
        setContentView(R.layout.activity_browse_pictures)

        val criteria = intent.getParcelableExtra(INTENT_EXTRA_SEARCH_SETTINGS) as SearchSettings?
        val viewAdapter = UnmonitoredFlickrRecyclerViewAdapter(arrayListOf())

        supportActionBar?.run {
            title = getString(R.string.title_activity_browse_unmonitored)
            subtitle = getString(R.string.browse_searching_tags, criteria?.tags)
            setDisplayHomeAsUpEnabled(true)
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@BrowsePicturesUnmonitoredActivity)
            adapter = viewAdapter
        }

        val viewModel = getViewModel<BrowsePicturesUnmonitoredViewModel>()

        viewModel.state.observe(this, Observer { newState ->
            when (newState) {
                is BrowsePicturesState.Error -> {
                    supportActionBar?.title = getString(R.string.title_activity_browse_unmonitored)
                    supportActionBar?.subtitle = null
                    viewAdapter.displayError(
                        getString(R.string.browse_error_searching, newState.errorMessage)
                    )
                    hide(nowLoading)
                }
                is BrowsePicturesState.Ok -> {
                    supportActionBar?.run {
                        title = getString(
                            R.string.title_activity_browse_results,
                            newState.images.size
                        )
                        subtitle = getString(
                            R.string.browse_images_found,
                            newState.settings.tags
                        )
                    }
                    viewAdapter.loadNewData(newState.images)
                    hide(nowLoading)
                }
                else -> show(nowLoading)
            }
        })

        viewModel.prepareSearch(criteria)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> true
    }

    override fun onBackPressed() = finish()
}