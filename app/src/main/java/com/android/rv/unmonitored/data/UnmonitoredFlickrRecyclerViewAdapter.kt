package com.android.rv.unmonitored.data

import android.view.View
import com.android.rv.R
import com.android.rv.data.FlickrImageViewHolder
import com.android.rv.data.FlickrRecyclerViewAdapter
import com.android.rv.data.Image
import com.android.rv.loadFrom
import com.android.rv.setTextOrHide

class UnmonitoredFlickrRecyclerViewAdapter(
    imageList: List<Image>
) : FlickrRecyclerViewAdapter(imageList) {
    override fun bindImage(holder: FlickrImageViewHolder, image: Image) = holder.run {
        thumbnail.loadFrom(image.link)
        title.text = image.title
        author.setTextOrHide(R.string.browse_author_format, image.author)
        tags.setTextOrHide(R.string.browse_tags_format, image.tags)
        separator.visibility = if (imageList.size < 2) View.GONE else View.VISIBLE
    }
}