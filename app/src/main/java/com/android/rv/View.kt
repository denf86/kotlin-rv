package com.android.rv

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import com.android.rv.data.Image
import com.android.rv.monitors.MonitoredComponent
import com.android.rv.monitors.MonitoredScope
import com.android.rv.monitors.async
import com.android.rv.monitors.launch
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URL
import kotlin.random.Random

fun randomDispatcher() = if (Random.nextBoolean()) Dispatchers.IO else Dispatchers.Main

fun ImageView.loadFrom(url: String?) =
    if (url.isNullOrBlank()) setBrokenLink()
    else {
        val handler = CoroutineExceptionHandler { _, t ->
            setBrokenLink()
            Log.d("loadFrom", "error loading $url: $t")
            GlobalScope.launch(Dispatchers.Main) { showToast(t.toString()) }
        }
        CoroutineScope(randomDispatcher() + handler).launch {
            setPlaceholder()
            withContext(randomDispatcher()) {
                delay(1_000L)
                BitmapFactory.decodeStream(URL(url).openStream())
            }.let { setImageBitmap(it) }
        }
        Unit
    }

fun ImageView.loadFrom(url: String?, component: MonitoredComponent) =
    if (url.isNullOrBlank()) setBrokenLink()
    else {
        val handler = CoroutineExceptionHandler { _, t ->
            Log.d("loadFrom", "error loading $url: $t")
            setBrokenLink()
        }
        MonitoredScope.launch(component, handler) {
            setPlaceholder()
            val bitmap = withContext(randomDispatcher()) {
                BitmapFactory.decodeStream(URL(url).openStream())
            }
            setImageBitmap(bitmap)
        }
        Unit
    }

fun ImageView.setBrokenLink() = setImageResource(R.drawable.brokenlink)
fun ImageView.setPlaceholder() = setImageResource(R.drawable.placeholder)

fun TextView.setTextOrHide(@StringRes format: Int, value: String?) =
    if (value == null) {
        visibility = View.GONE
    } else {
        visibility = View.VISIBLE
        text = context.getString(format, value)
    }

fun show(vararg view: View) = view.forEach { it.visibility = View.VISIBLE }
fun hide(vararg view: View) = view.forEach { it.visibility = View.GONE }

fun View.hideKeyboard() {
    val inputManager = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(windowToken, 0)
}

fun View.showToast(message: String) = Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

fun View.showLongToast(message: String) = Toast.makeText(context, message, Toast.LENGTH_LONG).show()