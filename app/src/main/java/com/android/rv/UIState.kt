package com.android.rv

import androidx.lifecycle.LiveData

interface UIState

interface StatefulViewModel<T : UIState> {
    val state: LiveData<T>
}