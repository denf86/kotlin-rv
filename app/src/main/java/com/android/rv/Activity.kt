package com.android.rv

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.android.rv.monitors.MonitoredActivity
import com.android.rv.monitors.MonitoredViewModel

inline fun <reified T : ViewModel> AppCompatActivity.getViewModel() =
    ViewModelProviders.of(this).get(T::class.java)

inline fun <reified T : MonitoredViewModel> MonitoredActivity.getMonitoredViewModel() =
    ViewModelProviders.of(this).get(T::class.java).also { it.init() }