package com.android.rv

import androidx.lifecycle.MutableLiveData

fun <T> MutableLiveData<T>.setTo(v: T) {
    value = v
}