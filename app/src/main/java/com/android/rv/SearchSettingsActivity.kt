package com.android.rv

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.rv.data.SearchMode
import com.android.rv.data.SearchSettings
import com.android.rv.properties.BrowsePicturesActivity
import com.android.rv.properties.INTENT_EXTRA_SEARCH_SETTINGS
import com.android.rv.unmonitored.BrowsePicturesUnmonitoredActivity
import kotlinx.android.synthetic.main.activity_search_settings.*

class SearchSettingsActivity : AppCompatActivity() {
    private val defaultTags = ""
    private val defaultSearchMode = R.id.optionAll
    private val defaultCompact = true

    override fun onCreate(savedInstanceState: Bundle?) = super.onCreate(savedInstanceState).also {
        setContentView(R.layout.activity_search_settings)

        inputTags.setText(defaultTags)
        optionSearchMode.check(defaultSearchMode)
        checkCompact.isChecked = defaultCompact

        buttonSearch.setOnClickListener {
            startActivity(searchIntent(BrowsePicturesActivity::class.java))
        }

        buttonSearchUnmonitored.setOnClickListener {
            startActivity(searchIntent(BrowsePicturesUnmonitoredActivity::class.java))
        }

        // Hide report UI as it's not in-scope yet.
        hide(buttonReport, labelReport, commentReport)
    }

    private fun <T> searchIntent(clazz: Class<T>) = Intent(this, clazz).apply {
        putExtra(
            INTENT_EXTRA_SEARCH_SETTINGS, SearchSettings(
                inputTags.text.toString(),
                if (optionAll.isChecked) SearchMode.ALL_TAGS else SearchMode.ANY_TAG,
                checkCompact.isChecked
            )
        )
    }
}