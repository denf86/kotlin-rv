package com.android.rv.monitors

import android.util.Log
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.ensureActive
import kotlinx.coroutines.launch
import kotlin.coroutines.ContinuationInterceptor
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

interface MonitoredComponent {
    val recommendedDispatchers: HashMap<String, CoroutineDispatcher>
    val defaultHandler: CoroutineExceptionHandler
    val coroutineScope: CoroutineScope

    val monitoredApplication: MonitoredApplication?

    // API ----------------------------------------------------------------------------------------

    /**
     * Initialises the MonitoredComponent's [recommendedDispatchers] with an empty map or,
     * when possible, with any entries found in the [monitoredApplication].
     */
    fun init() {
        recommendedDispatchers.clear()
        monitoredApplication?.loadRecommendedDispatchers(javaClass.name)?.let { entries ->
            recommendedDispatchers.putAll(entries)
        }
    }

    /**
     * When possible, saves any [recommendedDispatchers] into the [monitoredApplication].
     */
    fun destroy() {
        monitoredApplication?.saveRecommendedDispatchers(javaClass.name, recommendedDispatchers)
    }

    /**
     * Saves the [recommendedDispatcher] to be used whenever the task marked with [id] is launched.
     */
    fun saveRecommendedDispatcher(id: String, recommendedDispatcher: CoroutineDispatcher) =
        synchronized(recommendedDispatchers) {
            recommendedDispatchers.put(id, recommendedDispatcher)
        }

    /**
     * @return the recommended [CoroutineDispatcher] for the given [id] if it exists;
     * null otherwise.
     */
    fun getRecommendedDispatcher(id: String) =
        synchronized(recommendedDispatchers) { recommendedDispatchers[id] }

    /**
     * Generates a id that is subsequently used to recognise a task throughout its lifecycle.
     * @return "unique" caller id composed of className:methodName:lineNumber
     */
    fun buildTaskId() = makeStackTraceId()

    /**
     * Generic method called before a task (be it com.android.rv.monitors.launch or com.android.rv.monitors.async) is started.
     * Looks for a recommended dispatcher for the [taskId]; if a dispatcher is found
     * it's added to the [context].
     * @return updated CoroutineContext containing the recommended dispatcher if it exists,
     * the previous dispatcher if present and the default dispatcher otherwise.
     */
    fun beforeTask(context: CoroutineContext, taskId: String): CoroutineContext {
        val dispatcher = context[ContinuationInterceptor.Key] as CoroutineDispatcher?
        val newDispatcher = when (val recommended = getRecommendedDispatcher(taskId)) {
            null, dispatcher -> dispatcher ?: Dispatchers.Default
            else -> recommended
        }
        return context + newDispatcher
    }

    /**
     * Method called after beforeTask when launching a task with the [launch] method.
     * Handles any operations that are specific to com.android.rv.monitors.launch and not [async] such as
     * inserting a CoroutineExceptionHandler if none is present in the [context].
     * @return updated CoroutineContext containing the default handler if no
     * CoroutineExceptionHandler was already present; the new handler converts any
     * "wrong thread" exceptions to a WrongDispatcherException.
     */
    fun beforeLaunch(context: CoroutineContext, taskId: String): CoroutineContext {
        val handler = context[CoroutineExceptionHandler.Key].also {
            Log.d("launching", "$it")
        }
        val newHandler =
            if (handler is WrongDispatcherExceptionHandler) handler
            else WrongDispatcherExceptionHandler(handler ?: defaultHandler)
        return context + newHandler
    }

    /**
     * Method called after beforeTask when launching a task with the [async] method.
     * Handles any operations that are specific to com.android.rv.monitors.launch and not [launch] in theory;
     * in practice does nothing and should be overridden for more specific behaviour.
     * @return the [context] given as input.
     */
    fun beforeAsync(context: CoroutineContext, taskId: String): CoroutineContext {
        return context
    }

    /**
     * Method for handling a CancellationException. These exceptions are special cases
     * in coroutines and may need more specific behaviour.
     * The present implementation rethrows the exception, so it's strongly advised
     * to NOT call the super method when overriding.
     */
    fun handleCancellation(cancellationException: CancellationException, taskId: String): Nothing {
        throw cancellationException
    }

    /**
     * Method for handling a generic Exception.
     * The present implementation converts the [exception] to a WrongDispatcherException
     * and records a recommended CoroutineDispatcher for the given [taskId] when possible.
     * Either the original exception or the new WrongDispatcherException is thrown, so it's
     * strongly advised to NOT call the super method when overriding.
     */
    fun handleException(exception: Exception, taskId: String): Nothing =
        exception.toWrongDispatcherException().let { newException ->
            if (newException is WrongDispatcherException)
                saveRecommendedDispatcher(taskId, newException.recommendedDispatcher)
            throw newException
        }

    /**
     * Method called when the [task] has been started with either [launch] or [async].
     */
    fun afterTaskStart(task: Job, taskId: String) {}

    /**
     * Method called when the task identified by [taskId] is about to be terminated.
     */
    fun afterTaskEnd(taskId: String) {}

    // Utilities ----------------------------------------------------------------------------------

    /**
     * Stacktrace content:
     * [0] = this method
     * [1] = buildTaskId
     * [2] = caller method (usually com.android.rv.monitors.launch/com.android.rv.monitors.async)
     * [3] = default caller method e.g. com.android.rv.monitors.launch$default
     * [4] = actual invokeSuspend call
     * @return "unique" caller id composed of className:methodName:lineNumber
     */
    @Suppress("NOTHING_TO_INLINE")// Have to inline for stacktrace accuracy.
    private inline fun makeStackTraceId() = Exception().stackTrace[4].let {
        "${it.className}.${it.methodName}:${it.lineNumber}"
    }
}

/**
 * Monitored version of [CoroutineScope].[launch]:
 * (1) the [context] is processed with a generic control method;
 * (2) the resulting context is further processed with a [launch]-specific control method;
 * (3) the [block] is launched on the [component]'s own CoroutineScope using the updated context;
 * (4) any CancellationException is handled separately;
 * (5) any other Exception is handled by a common method;
 * (6) the [component] is notified that the task has been launched;
 * (7) the [component] will be notified when the task is terminated.
 */
fun CoroutineScope.launch(
    component: MonitoredComponent,
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
): Job {
    // Generate id right away.
    val id = component.buildTaskId()
    // Perform pre-com.android.rv.monitors.launch operations.
    val newContext = with(component) {
        beforeLaunch(beforeTask(coroutineContext + context, id), id)
    }
    // Launch using the component's own scope.
    val task = component.coroutineScope.launch(newContext, start) {
        Log.d("launch", "$newContext")
        Log.d("launch", "$coroutineContext")
        try {
            ensureActive()
            block()
        } catch (c: CancellationException) {
            // Handle specific CancellationException.
            component.handleCancellation(c, id)
        } catch (e: Exception) {
            // Handle generic Exception.
            component.handleException(e, id)
        } finally {
            component.afterTaskEnd(id)
        }
    }
    component.afterTaskStart(task, id)
    return task
}

/**
 * Monitored version of [CoroutineScope].[async]:
 * (1) the [context] is processed with a generic control method;
 * (2) the resulting context is further processed with a [async]-specific control method;
 * (3) the [block] is launched on the [component]'s own CoroutineScope using the updated context;
 * (4) any CancellationException is handled separately;
 * (5) any other Exception is handled by a common method;
 * (6) the [component] is notified that the task has been launched;
 * (7) the [component] will be notified when the task is terminated.
 */
fun <T> CoroutineScope.async(
    component: MonitoredComponent,
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> T
): Deferred<T> {
    // Generate id right away.
    val id = component.buildTaskId()
    // Perform pre-com.android.rv.monitors.launch operations.
    val newContext = with(component) {
        beforeAsync(beforeTask(coroutineContext + context, id), id)
    }
    // Launch using the component's own scope.
    val task = component.coroutineScope.async(newContext, start) {
        try {
            ensureActive()
            block()
        } catch (c: CancellationException) {
            // Handle specific CancellationException.
            component.handleCancellation(c, id)
        } catch (e: Exception) {
            // Handle generic Exception.
            component.handleException(e, id)
        } finally {
            component.afterTaskEnd(id)
        }
    }
    component.afterTaskStart(task, id)
    return task
}