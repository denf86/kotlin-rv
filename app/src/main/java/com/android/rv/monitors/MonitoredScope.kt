@file:Suppress("unused")

package com.android.rv.monitors

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * Empty object that serves as syntactic sugar for calling an instrumented
 * [launch] or [async] on a MonitoredComponent using the component's own scope.
 */
object MonitoredScope

fun <ComponentType : MonitoredComponent> MonitoredScope.launch(
    component: ComponentType,
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
) = component.coroutineScope.launch(component, context, start, block)

fun <ComponentType : MonitoredComponent> MonitoredScope.async(
    component: ComponentType,
    context: CoroutineContext = EmptyCoroutineContext,
    start: CoroutineStart = CoroutineStart.DEFAULT,
    block: suspend CoroutineScope.() -> Unit
) = component.coroutineScope.async(component, context, start, block)