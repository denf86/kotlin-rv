package com.android.rv.monitors

import android.os.NetworkOnMainThreadException
import android.util.Log
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/**
 * Exception type used for masking failures caused by a task being run on the "wrong"
 * [CoroutineDispatcher].
 * The [recommendedDispatcher] identifies the thread(s) in which the execution should
 * have been carried out.
 */
class WrongDispatcherException(
    cause: Throwable,
    val recommendedDispatcher: CoroutineDispatcher
) : Exception(cause) {
    override fun toString(): String = "${javaClass.name} (${cause?.javaClass?.simpleName})"
}

/**
 * Wraps exceptions of select types inside a [WrongDispatcherException], while leaving
 * any other objects untouched.
 * @return new instance of [WrongDispatcherException] if receiver is either a
 * [NetworkOnMainThreadException] or a CalledFromWrongThreadException; the receiver
 * itself in any other case.
 */
fun <E : Throwable> E.toWrongDispatcherException(): Throwable = when {
    this is NetworkOnMainThreadException -> WrongDispatcherException(this, Dispatchers.IO)
    this.isCalledFromWrongThreadException -> WrongDispatcherException(this, Dispatchers.Main)
    else -> this
}

/**
 * Custom [CoroutineExceptionHandler] that wraps exceptions of select types inside a
 * [WrongDispatcherException] before handling them.
 */
class WrongDispatcherExceptionHandler(
    private val handler: CoroutineExceptionHandler
) : CoroutineExceptionHandler {
    override val key = handler.key

    override fun handleException(context: CoroutineContext, exception: Throwable) {
        Log.e("ExceptionHandler", "Handling $exception...")
        handler.handleException(context, exception.toWrongDispatcherException())
    }
}

/**
 * Since the exception type CalledFromWrongThreadException is not exposed,
 * this accessor acts as the type check: receiver is CalledFromWrongThreadException.
 * @return true if receiver is an instance of CalledFromWrongThreadException;
 * false otherwise.
 */
val Throwable.isCalledFromWrongThreadException
    get() = javaClass.name == "android.view.ViewRootImpl\$CalledFromWrongThreadException"