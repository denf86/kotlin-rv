package com.android.rv.monitors

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.CoroutineDispatcher

class TerminationService : Service() {
    override fun onBind(intent: Intent?): IBinder? = null

    override fun onTaskRemoved(rootIntent: Intent?) = super.onTaskRemoved(rootIntent).also {
        application.monitored?.let { writeReport(it.recommendedDispatchers) }
        stopSelf()
    }

    private fun writeReport(finalMap: HashMap<String, HashMap<String, CoroutineDispatcher>>) {
        if (finalMap.isNotEmpty()) {
            val builder = StringBuilder("Post-execution report for app com.android.rv.KotlinRV:\n")
            finalMap.entries.forEach { classEntry ->
                builder.append("Component: ${classEntry.key}\n")
                classEntry.value.entries.forEach { taskEntry ->
                    builder.append("\t${taskEntry.key} => ${taskEntry.value}\n")
                }
            }
            Log.d("Report", builder.toString())
        }
    }
}