package com.android.rv.monitors

import android.app.Application
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlin.coroutines.CoroutineContext

abstract class MonitoredActivity : AppCompatActivity(), MonitoredComponent {
    override val recommendedDispatchers = hashMapOf<String, CoroutineDispatcher>()
    override val coroutineScope = lifecycleScope

    override val monitoredApplication: MonitoredApplication?
        get() = application.monitored

    override fun onCreate(savedInstanceState: Bundle?) = super.onCreate(savedInstanceState).also {
        init()
    }

    override fun onDestroy() = super.onDestroy().also {
        destroy()
    }
}

abstract class MonitoredViewModel(
    application: Application
) : AndroidViewModel(application), MonitoredComponent {
    override val recommendedDispatchers = hashMapOf<String, CoroutineDispatcher>()
    override val coroutineScope = viewModelScope

    override val monitoredApplication: MonitoredApplication?
        get() = getApplication<Application>().let {
            if (it is MonitoredApplication) it
            else null
        }

    private var initialised = false

    override fun init() = super.init().also {
        initialised = true
    }

    override fun onCleared() = super.onCleared().also {
        destroy()
    }

    override fun beforeTask(context: CoroutineContext, taskId: String): CoroutineContext {
        if (!initialised) init()
        return super.beforeTask(context, taskId)
    }
}