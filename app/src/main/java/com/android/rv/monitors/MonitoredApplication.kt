package com.android.rv.monitors

import android.app.Application
import android.content.Intent
import kotlinx.coroutines.CoroutineDispatcher

/**
 * Custom [Application] subclass that keeps track of the [recommendedDispatchers]
 * for each monitored component.
 * The dispatchers are stored using the component's Java class name (e.g.
 * com.android.rv.properties.BrowsePicturesViewModel).
 */
class MonitoredApplication : Application() {
    internal val recommendedDispatchers = hashMapOf<String, HashMap<String, CoroutineDispatcher>>()

    override fun onCreate() = super.onCreate().also {
        startService(Intent(applicationContext, TerminationService::class.java))
    }

    /**
     * @return recommended coroutine dispatchers for the component
     * identified with the given [className].
     */
    fun loadRecommendedDispatchers(
        className: String
    ) = recommendedDispatchers[className]

    /**
     * Saves the given [entries] using the [className] as key for future use.
     */
    fun saveRecommendedDispatchers(
        className: String,
        entries: HashMap<String, CoroutineDispatcher>
    ) = recommendedDispatchers.put(className, entries)
}

val Application.monitored
    get() = if (this is MonitoredApplication) this else null