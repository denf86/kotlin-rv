package com.android.rv.annotation

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class Logged(val tag: String, val message: String)