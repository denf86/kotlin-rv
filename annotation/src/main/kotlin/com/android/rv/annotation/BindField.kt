package com.android.rv.annotation

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.FUNCTION)
annotation class BindField(val viewIds: Array<String>, val viewName: String)