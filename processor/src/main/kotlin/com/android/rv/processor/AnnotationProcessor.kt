package com.android.rv.processor

import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.asTypeName
import java.io.File
import javax.annotation.processing.AbstractProcessor
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ExecutableElement
import javax.tools.Diagnostic

// AnnotationProcessor abstract class =============================================================

abstract class AnnotationProcessor(vararg supportedAnnotationTypes: String) : AbstractProcessor() {
    private val _supportedAnnotationTypes = supportedAnnotationTypes.toMutableSet()

    override fun getSupportedAnnotationTypes() = _supportedAnnotationTypes

    override fun getSupportedSourceVersion(): SourceVersion = SourceVersion.latest()

    protected fun errorMessage(message: String) =
        processingEnv.messager.printMessage(Diagnostic.Kind.ERROR, message)

    protected val ExecutableElement.packageName
        get() = processingEnv.elementUtils.getPackageOf(this).toString()

    companion object {
        const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"
    }
}

// KotlinPoet utility functions ===================================================================

fun FunSpec.Builder.addParameters(vararg params: Pair<String, TypeName>) =
    params.forEach { addParameter(it.first, it.second) }

fun File.writeSpec(
    packageName: String,
    fileName: String,
    setup: FileSpec.Builder.() -> FileSpec.Builder
) = FileSpec.builder(packageName, fileName).run(setup).build().writeTo(this)

// TypeName constructors --------------------------------------------------------------------------

fun typeName(element: Element): TypeName = element.asType().asTypeName()

inline fun <reified T> typeName(): TypeName = T::class.asTypeName()

// File utility functions =========================================================================

fun file(rootPath: String, setup: File.() -> Unit) = File(rootPath).apply(setup)

infix fun String.asFile(setup: File.() -> Unit) = file(this, setup)