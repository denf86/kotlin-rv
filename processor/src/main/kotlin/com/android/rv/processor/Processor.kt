package com.android.rv.processor

import com.android.rv.annotation.Logged
import com.android.rv.processor.AnnotationProcessor.Companion.KAPT_KOTLIN_GENERATED_OPTION_NAME
import com.google.auto.service.AutoService
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeSpec
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.annotation.processing.SupportedOptions
import javax.annotation.processing.SupportedSourceVersion
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement

@AutoService(Processor::class)
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedOptions(KAPT_KOTLIN_GENERATED_OPTION_NAME)
class Processor : AnnotationProcessor(Logged::class.java.canonicalName) {
    override fun process(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Boolean {
        roundEnv.getElementsAnnotatedWith(Logged::class.java).forEach { classElement ->
            when {
                classElement.kind != ElementKind.CLASS ->
                    errorMessage("Can only be applied to classes: $classElement")
                else -> generateLoggingMethod(
                    classElement, processingEnv.elementUtils.getPackageOf(classElement).toString()
                )
            }
        }
        return false
    }

    private fun generateLoggingMethod(className: Element, packageName: String) {
        val generatedRoot = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]
        if (generatedRoot == null) {
            errorMessage("Can't find the target directory for generated kotlin files.")
            return
        }

        val annotation: Logged = className.getAnnotation(Logged::class.java)
        val fileName = "${className.simpleName}Logger"
        val contextPackager = ClassName("androidx.appcompat.app", "AppCompatActivity")
        val callerMethod = FunSpec.builder("start${className.simpleName}").apply {
            addParameters(
                "activity" to contextPackager,
                "resourceIdToBeLogged" to typeName<Int>()
            )
//            returns(Int::class)
            addCode(
                """android.util.Log.d(
                    "${annotation.tag}", 
                    "${annotation.message} " + activity.toString() + " id: " + resourceIdToBeLogged
                )""".trimIndent()
            )
        }.build()
        val generatedClass = TypeSpec.objectBuilder(fileName).addFunction(callerMethod).build()
        generatedRoot asFile {
            mkdir()
            writeSpec(packageName, "$fileName.kt") {
                addType(generatedClass)
            }
        }
/*
        // Reading properties
        val variableAsElement: Element = processingEnv.typeUtils.asElement(variable.asType())
        val fieldsInArgument: List<VariableElement> =
            ElementFilter.fieldsIn(variableAsElement.enclosedElements)


        // Building function
        val funcBuilder = FunSpec.builder("logThings").apply {
            addModifiers(KModifier.PUBLIC)
            addParameter(variable.simpleName.toString(), typeName(variableAsElement))
            addStatement(
                "android.util.Log.d(\"%L\", \"%L\" + %L.toString())",
                annotation.tag,
                annotation.message,
                variable.simpleName
            )
        }
//        annotation.viewIds.forEachIndexed { index, viewId ->
//            funcBuilder.addStatement(
//                "%L.findViewById<%T>(R.id.%L).text = %L.%L",
//                annotation.viewName,
//                ClassName("android.widget", "TextView"),
//                viewId,
//                variable.simpleName,
//                fieldsInArgument[index].simpleName
//            )
//        }

        // Generating file
        generatedRoot asFile {
            mkdir()
            writeSpec(packageName, "LoggedGenerated") {
                addFunction(funcBuilder.build())
            }
        }*/
    }
}

