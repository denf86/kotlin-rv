package com.android.benchmark

import androidx.benchmark.junit4.BenchmarkRule
import androidx.test.rule.ActivityTestRule
import com.android.rv.SearchSettingsActivity
import com.android.rv.properties.BrowsePicturesActivity
import com.android.rv.unmonitored.BrowsePicturesUnmonitoredActivity
import kotlinx.coroutines.delay
import org.junit.Rule

abstract class BaseBenchmark {
    @get:Rule
    val benchmarkRule = BenchmarkRule()

    @get:Rule
    var browseMonitored = ActivityTestRule(BrowsePicturesActivity::class.java)

    @get:Rule
    var browseUnmonitored = ActivityTestRule(BrowsePicturesUnmonitoredActivity::class.java)

    @get:Rule
    var searchSettings = ActivityTestRule(SearchSettingsActivity::class.java)

    suspend fun runSomething() = delay(100L)
}