package com.android.benchmark

import android.graphics.BitmapFactory
import android.util.Log
import androidx.benchmark.junit4.measureRepeated
import androidx.lifecycle.lifecycleScope
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.android.rv.customFlickrUri
import com.android.rv.getMonitoredViewModel
import com.android.rv.getViewModel
import com.android.rv.monitors.MonitoredScope
import com.android.rv.monitors.async
import com.android.rv.monitors.launch
import com.android.rv.parseFlickrImageJson
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import java.net.URL

@RunWith(AndroidJUnit4::class)
class LaunchBenchmark : BaseBenchmark() {
    @Test
    fun testMonitoredLaunch() = browseMonitored.activity.let { activity ->
        benchmarkRule.measureRepeated {
            val task = MonitoredScope.launch(activity) { runSomething() }
            runBlocking { task.join() }
        }
    }

    @Test
    fun testUnmonitoredLaunch() = browseUnmonitored.activity.let { activity ->
        benchmarkRule.measureRepeated {
            val task = activity.lifecycleScope.launch { runSomething() }
            runBlocking { task.join() }
        }
    }
}

@RunWith(AndroidJUnit4::class)
class AsyncBenchmark : BaseBenchmark() {
    @Test
    fun testMonitoredAsync() = browseMonitored.activity.let { activity ->
        benchmarkRule.measureRepeated {
            val result = MonitoredScope.async(activity) { runSomething() }
            runBlocking { result.await() }
        }
    }

    @Test
    fun testUnmonitoredAsync() = browseUnmonitored.activity.let { activity ->
        benchmarkRule.measureRepeated {
            val result = activity.lifecycleScope.async { runSomething() }
            runBlocking { result.await() }
        }
    }
}

@RunWith(AndroidJUnit4::class)
class ViewModelBenchmark : BaseBenchmark() {
    private val flickrUri = customFlickrUri("lamborghini", "any")

    @Test
    fun testMonitoredViewModel() =
        browseMonitored.activity.getMonitoredViewModel<BrowsePicturesViewModel>().let { viewModel ->
            benchmarkRule.measureRepeated {
                MonitoredScope.launch(viewModel, Dispatchers.IO) {
                    // Read JSON data from Flickr
                    val json = URL(flickrUri).readText()
                    val parsedData = parseFlickrImageJson(json, false)
                    // Get each picture as a bitmap
                    parsedData.map {
                        it.getOrThrow().link.let {
                            async(viewModel) { BitmapFactory.decodeStream(URL(it).openStream()) }
                        }
                    }.awaitAll()
                }
            }
            browseMonitored.finishActivity()
        }

    @Test
    fun testUnmonitoredViewModel() =
        browseUnmonitored.activity.getViewModel<BrowsePicturesUnmonitoredViewModel>().let { viewModel ->
            val handler = CoroutineExceptionHandler { _, t ->
                Log.e("Benchmark", "Failure", t)
            }
            benchmarkRule.measureRepeated {
                viewModel.viewModelScope.launch(Dispatchers.IO + handler) {
                    // Read JSON data from Flickr
                    val json = URL(flickrUri).readText()
                    val parsedData = parseFlickrImageJson(json, false)
                    // Get each picture as a bitmap
                    parsedData.map {
                        it.getOrThrow().link.let {
                            async { BitmapFactory.decodeStream(URL(it).openStream()) }
                        }
                    }.awaitAll()
                }
            }
            browseUnmonitored.finishActivity()
        }

}

@RunWith(AndroidJUnit4::class)
class MassiveBenchmark : BaseBenchmark() {
    private val maxTasks = 1_000

    @Test
    fun testMonitoredMassiveAsync() = browseMonitored.activity
        .getMonitoredViewModel<BrowsePicturesViewModel>().let { viewModel ->
            benchmarkRule.measureRepeated {
                val deferred = (1..maxTasks).map {
                    MonitoredScope.async(viewModel) { runSomething() }
                }
                runBlocking { deferred.awaitAll() }
            }
            browseMonitored.finishActivity()
        }

    @Test
    fun testUnmonitoredMassiveAsync() = browseUnmonitored.activity
        .getViewModel<BrowsePicturesUnmonitoredViewModel>().let { viewModel ->
            benchmarkRule.measureRepeated {
                val deferred = (1..maxTasks).map {
                    viewModel.viewModelScope.async { runSomething() }
                }
                runBlocking { deferred.awaitAll() }
            }
            browseUnmonitored.finishActivity()
        }
}